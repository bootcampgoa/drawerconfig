<?php
// Our custom post type function
function create_posttype() {
//registers a post type called Orders 
	//second argument is array..which has two parts..first array takes labels and name and second takes oder para like public ,has archive
	register_post_type( 'orders',
	// CPT Options
		array(
			'labels' => array(
				'name' => __( 'Orders' ),
				'singular_name' => __( 'Order' )
			),
			'description'=> __( 'order types take away and have here', 'customtemplate' ),
			'public' => true,
			'has_archive' => true,
			'rewrite' => array('slug' => 'orders'),
		)
	);
}

// Hooking up our function to theme setup
add_action( 'init', 'create_posttype' );

/**
 * Add custom taxonomies
 *
 * Additional custom taxonomies can be defined here
 * http://codex.wordpress.org/Function_Reference/register_taxonomy
 */
function add_custom_taxonomies() {
  // Add new "Locations" taxonomy to Posts
  register_taxonomy('take away', 'orders', array(
    // Hierarchical taxonomy (like categories)
    'hierarchical' => true,
    // This array of options controls the labels displayed in the WordPress Admin UI
    'labels' => array(
      'name' => _x( 'take away orders', 'take away order type' ),
      'singular_name' => _x( 'take away order', 'take away order' ),
      'search_orders' =>  __( 'Search Ordrers' ),
      'all_orders' => __( 'All Orders' ),
      'parent_orders' => __( 'Orders' ),
      'parent_orders_colon' => __( 'Parent Location:' ),
      'edit_orders' => __( 'Edit Order' ),
      'update_orders' => __( 'Update Order' ),
      'add_new_orders' => __( 'Add New Order' ),
      'new_orders' => __( 'New Order Name' ),
      'menu_name' => __( 'take away' ),
    ),
    // Control the slugs used for this taxonomy
    'rewrite' => array(
      'slug' => 'takeaway', // This controls the base slug that will display before each term
      'with_front' => false, // Don't display the category base before "/locations/"
      'hierarchical' => true // This will allow URL's like "/locations/boston/cambridge/"
    ),
  ));

	register_taxonomy('have here', 'orders', array(
    // Hierarchical taxonomy (like categories)
    'hierarchical' => true,
    // This array of options controls the labels displayed in the WordPress Admin UI
    'labels' => array(
      'name' => _x( 'have here orders', 'have here order type' ),
      'singular_name' => _x( 'have here order', 'have here order' ),
      'search_orders' =>  __( 'Search Ordrers' ),
      'all_orders' => __( 'All Orders' ),
      'parent_orders' => __( 'Orders' ),
      'parent_orders_colon' => __( 'Parent Location:' ),
      'edit_orders' => __( 'Edit Order' ),
      'update_orders' => __( 'Update Order' ),
      'add_new_orders' => __( 'Add New Order' ),
      'new_orders' => __( 'New Order Name' ),
      'menu_name' => __( 'have here' ),
    ),
    // Control the slugs used for this taxonomy
    'rewrite' => array(
      'slug' => 'havehere', // This controls the base slug that will display before each term
      'with_front' => false, // Don't display the category base before "/locations/"
      'hierarchical' => true // This will allow URL's like "/locations/boston/cambridge/"
    ),
  ));
}

add_action( 'init', 'add_custom_taxonomies', 0 );


?>
