$('.form').find('input, textarea').on('keyup blur focus', function (e) {
  
  var $this = $(this),
      label = $this.prev('label');

	  if (e.type === 'keyup') {
			if ($this.val() === '') {
          label.removeClass('active highlight');
        } else {
          label.addClass('active highlight');
        }
    } else if (e.type === 'blur') {
    	if( $this.val() === '' ) {
    		label.removeClass('active highlight'); 
			} else {
		    label.removeClass('highlight');   
			}   
    } else if (e.type === 'focus') {
      
      if( $this.val() === '' ) {
    		label.removeClass('highlight'); 
			} 
      else if( $this.val() !== '' ) {
		    label.addClass('highlight');
			}
    }

});

$('.tab a').on('click', function (e) {
  
  e.preventDefault();
  
  $(this).parent().addClass('active');
  $(this).parent().siblings().removeClass('active');
  
  target = $(this).attr('href');

  $('.tab-content > div').not(target).hide();
  
  $(target).fadeIn(600);
  
});


$("#submitbutton").click(function(event){

  // alert(un);
// alert("in");
 $.ajax( {
        type: "POST",
        url: ajaxurl,
        data: {action:'get_registeruser',formdata:$('#registerform').serialize()},
        success: function( response ) {
        var obj = JSON.parse(response);
         
        if(obj.fill==1){
            $("#uname").css("border","1px solid red");
            $("#email").css("border","1px solid red");
            $("#password").css("border","1px solid red");
        }
        else if(obj.fill==5){
          $("#email").css("border","1px solid red");
        }
        else if(obj.fill==20){
          $("#uname").css("border","1px solid red");
        }
        else if(obj.fill==25){
          $("#password").css("border","1px solid red");
        }
       

        if(obj.userexists==0){
           // $('#registerform')[0].reset();
            $("#registerform").trigger("reset");

         $("#showregistermsg").html(obj.data);

           
            // $( '#registerform' )[0].each(function(){
            //     this.reset();
            // });
      
              }
        else{
           // $(obj.data).appendTo('#signup');
            $("#showregistermsg").html(obj.data);
           
        }
      
       },
    });
});



// LOGIN 
$("#loginbutton").click(function(event){
  // var un=$("#uname").val();
  // alert(un);
// alert("in");
 $.ajax( {
        type: "POST",
        url: ajaxurl,
        data: {action:'get_login',formdata:$('#loginform').serialize()},
        success: function( response ) {
        var obj = JSON.parse(response);
        // alert(obj.fill);
         // window.open('http://127.0.0.1/drawerconfig/?page_id=325');
         if(obj.fill==1){
            $("#loginuname").css("border","1px solid red");
            $("#loginpass").css("border","1px solid red");
        }
        else if(obj.fill==10){
          $("#loginuname").css("border","1px solid red");
          $("#loginpass").css("border","1px solid red");
        }
        //  if(obj.fill==1){
        //     $("#loginuname").css("border","1px solid red");
        //     $("#loginpass").css("border","1px solid red");
        // }

         if(obj.status==0){
           window.location=obj.data;
         }
         else{
            // $(obj.data).appendTo('#signup');
           $("#showloginmsg").html(obj.data);
         }
        
       },
    });
});

// LOGOUT
$("#logout").click(function(event){
  // var un=$("#uname").val();
  // alert(un);
// alert("in");
 $.ajax( {
        type: "POST",
        url: ajaxurl,
        data: {action:'get_logout'},
        success: function( response ) {
        var obj = JSON.parse(response);
        // alert(obj.data);
          window.location=obj.data;
        
        
       },
    });
});



//Thank you page
// $("#orderbutton").click(function(event){
//   // var un=$("#uname").val();
//   // alert(un);
//   // alert("order");
//    window.location="http://127.0.0.1/drawerconfig/?page_id=700";
// });


$("#successorder").click(function(event){
  // alert("in");
  window.location="http://127.0.0.1/drawerconfig/?page_id=325";
});

