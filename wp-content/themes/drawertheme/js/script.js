$(document).ready()
{

// $('.form input[type="text"]').attr('style', '-webkit-box-shadow: inset 0 0 0 1000px #fafafa !important');
// $('.form input[type="password"]').attr('style', '-webkit-box-shadow: inset 0 0 0 1000px #fafafa !important');







$height=$(document).height(); 
$width=$(document).width();
if($width<768){
  $(".form").css("height",$height+80+"px");

}
else{
  $(".form").css("height","auto");
}

// if($width>=1200 && $width<1400){
//   $.css("transform", "");
// }


if($width>767){
   $(".form").css("transform","translate(0%,30%)");
}

if($width>992 && $width<1200){
   $(".form").css("transform","translate(0%,50%)");
}

if($width>1400){
   $(".form").css("transform","translate(0%,50%)");
}

// $("#submitbutton").click(function(){
//   $height=$(document).height(); 

// })
// $(".form").css("height","auto");

$("#steptwo").children().bind('click', function(){ return false; });


  // alert($(window).width());
  (function($) {
    var element = $('.summery-box'),
        originalY = element.offset().top;
    
    // Space between element and top of screen (when scrolling)
    var topMargin = 20;
    
    // Should probably be set in CSS; but here just for emphasis
    element.css('position', 'relative');
    
    $(window).on('scroll', function(event) {
        var scrollTop = $(window).scrollTop();
        
        element.stop(false, false).animate({
            top: scrollTop < originalY
                    ? 0
                    : scrollTop - originalY + topMargin
        }, 500);
    });
})(jQuery);


	
	// $('#stepone').css('cursor', 'pointer');
	// $('#steptwo').css('cursor', 'pointer');
	// $('#stepthree').css('cursor', 'pointer');
	
	// previous step
    $('.form-wizard #stepone').on('click', function() {

	// navigation steps / progress steps
	var current_active_step = $(this).parents('.form-wizard').find('.form-wizard-step.active');
	var progress_line = $(this).parents('.form-wizard').find('.form-wizard-progress-line');
    	
	current_active_step.removeClass('active');
 	$("#steptwofields").hide();
 	$("#stepthreefields").hide();
 	$("#stepfourfields").hide();
	$("#fws1").addClass('active');
   	$("#steponefields").show();
    	
    	
    });
    
       // previous step
    $('#steptwo').on('click', function() {

	// navigation steps / progress steps
	var current_active_step = $(this).parents('.form-wizard').find('.form-wizard-step.active');
	var progress_line = $(this).parents('.form-wizard').find('.form-wizard-progress-line');
    	
	current_active_step.removeClass('active');
 	$("#stepthreefields").hide();
 	$("#stepfourfields").hide();
 	$("#steponefields").hide();
	$("#fws2").addClass('active');
 	$("#steptwofields").show();
    	
    	
    });
    
    // previous step
    $('#stepthree').on('click', function() {
   
	// navigation steps / progress steps
	var current_active_step = $(this).parents('.form-wizard').find('.form-wizard-step.active');
	var progress_line = $(this).parents('.form-wizard').find('.form-wizard-progress-line');
	
	current_active_step.removeClass('active');
 	$("#stepfourfields").hide();
 	$("#steptwofields").hide();
 	$("#steponefields").hide();
	$("#fws3").addClass('active');
   	$("#stepthreefields").show();
    	
    	
    });
   


var model="";

$(".product").change(function(e){ 
$(this).parent().parent().parent().parent().siblings().children().removeClass('highlight',this.checked);
$(this).parent().parent().parent().toggleClass('highlight', this.checked);

var model=$("input[name='model']:checked").parent('label').text();

localStorage.setItem("model", model);
 if (localStorage.getItem("model") === null) {
 
    $("#summarypage1").css("visibility", "visible");
    $('<p class="summery-type"></p><p class="summery-text modeltext">MODEL:</p><p class="summery-subtext modelsubtext">'+localStorage.getItem("model")+'</p>').appendTo('#pageoneconf');

}
else{
    
    $("#steptwo").prop("disabled", true);
    $("#stepthree").prop("disabled", true);
    $("#stepfour").prop("disabled", true);
    $(".greenbtnstep1").prop("disabled", true);
    // $("#trayconfigdiv").remove();
    // $("#traydepthdiv").remove();
    // $("#frontfixingdiv").remove();
    // $("#otheroptionsdiv").remove();

    $("#summarypage1").css("visibility", "visible");
    $(".modeltext").remove();
    $(".modelsubtext").remove();
    $(".colortext").remove();
    $(".colorsubtext").remove();
    $('<p class="summery-type"></p><p class="summery-text modeltext">MODEL:</p><p class="summery-subtext modelsubtext">'+localStorage.getItem("model")+'</p>').appendTo('#pageoneconf');
}

  
  $('#colorload').show();
  
    $.ajax( {
        type: "POST",
        url: ajaxurl,
        data: {action:'get_colors',params:$(e.target).val()},
        success: function( response ) {

    var obj = JSON.parse(response);

   $('html, body').animate({scrollTop:$('#colordiv').position().top}, 'slow');
    $("#colordiv").html(obj.data).hide().html(obj.data).fadeIn();
	
        },
         complete: function(){
        $('#colorload').hide();
    	}
    });

});



$(document).on('click', '.colors', function (event) {

$(this).parent().parent().parent().parent().siblings().children().removeClass('highlight',this.checked);
$(this).parent().parent().parent().toggleClass('highlight', this.checked);

 if($('.product').is(':checked')) { 
$(".greenbtnstep1").prop("disabled", false);
 }


var color=$("input[name='color']:checked").parent('label').text();
  localStorage.setItem("color", color);
   if (localStorage.getItem("model") === null) {
    $('<p class="summery-text colortext">COLOR:</p><p class="summery-subtext colorsubtext">'+localStorage.getItem("color")+'</p>').appendTo('#pageoneconf');
    }
    else{
        $(".colortext").remove();
        $(".colorsubtext").remove();
        $('<p class="summery-text colortext">COLOR:</p><p class="summery-subtext colorsubtext">'+localStorage.getItem("color")+'</p>').appendTo('#pageoneconf');
    }
});



$(document).on('click', '.traytype', function (event) {

$(this).parent().parent().parent().parent().siblings().children().removeClass('highlight',this.checked);
$(this).parent().parent().parent().toggleClass('highlight', this.checked);
$(".greenbtnstep3").prop("disabled", false);

});



$(document).on('click', '.oo', function (event) {

$(this).parent().parent().parent().parent().siblings().children().removeClass('highlight',this.checked);
$(this).parent().parent().parent().toggleClass('highlight', this.checked);
});




//to retrive tray type
$(".greenbtnstep1").click(function(e){ 
    $("#steptwo").prop("disabled", false);
var color=$("input[name='color']:checked").parent('label').text();
  localStorage.setItem("color", color);
  $("#traydiv").empty();


var abc=$('input[name=color]:checked').attr('data-trayid');
// Model Series
var a=$("#conf").append("<div id='configdiv' class='configdiv'><p class='summery-type'></p></div>");
  localStorage.setItem("div",a);

// if ($(".summery-box .model2").length > 0){ 
    $(".model2").remove();
    $(".model2sub").remove();
    $(".color2").remove();
    $(".color2sub").remove();
$('<p class="summery-text model2">MODEL:</p><p class="summery-subtext model2sub">'+localStorage.getItem("model")+'</p>').appendTo('#configdiv');
$('<p class="summery-text color2">COLOR:</p><p class="summery-subtext color2sub">'+localStorage.getItem("color")+'</p>').appendTo('#configdiv');

  $(".traytypetext").remove();
  $(".traytypesummary").remove();
  $(".traytypesubtext").remove();
  $(".greenbtnstep3").prop("disabled", true); 
  $("#stepthree").prop("disabled", true); 



$('#trayload').show();
    $.ajax( {
        type: "POST",
        url: ajaxurl,
        data: {action:'get_traytype',params:abc},
        success: function( response ) {

    var obj = JSON.parse(response);
     $('html, body').animate({scrollTop:$('#traydiv').position().top}, 'slow');
    $("#traydiv").html(obj.data).hide().html(obj.data).fadeIn();
     
        },
        complete: function(){
        $('#trayload').hide();
    	}
    });
    

});


//to retrive tray type on second page 
$(document).on('click', '.traytype', function (event) {

var fetchdepth=$('input[name=trayconfig]:checked').attr('data-traydepth');
// $("input[name=trayconfig]").prop("checked", true);
  $("#stepthree").prop("disabled", true); 

var a=$("#conf").append("<div id='configdiv1' class><p class='summery-type traytypesummary'>Tray Type</p></div>");
  localStorage.setItem("div",a);

 var traytype=$("input[name='traytype']:checked").parent('label').text();
 localStorage.setItem("traytype", traytype);

  // var rad=localStorage.getItem("traytype");
 // alert(rad);

  // if (localStorage.getItem("traytype") === null) {
  //       $('<p class="summery-text traytypetext">TRAY TYPE:</p><p class="summery-subtext traytypesubtext">'+localStorage.getItem("traytype")+'</p>').appendTo('#configdiv1');
  // }
  // else{
    $(".traytypesummary").remove();
    $(".traytypetext").remove();
    $(".traytypesubtext").remove();
    $('<p class="summery-text traytypetext">TRAY TYPE:</p><p class="summery-subtext traytypesubtext">'+localStorage.getItem("traytype")+'</p>').appendTo('#configdiv1');
    // $(".traytype").attr('checked', 'checked');
// $('input[name=traytype][value=' + rad + ']').prop('checked',true);
  // }

 
 

});  



$(document).on('click', '#prevbuttontwo', function (event) {
  // alert("back");
  $(".traytypetext").remove();
  $(".traytypesummary").remove();
  $(".traytypesubtext").remove();
  $(".greenbtnstep3").prop("disabled", true); 
  
}); 



//to retrive tray config on third page 
$(document).on('click', '.getdepth', function (event) {

 var trayconfig=$("input[name='trayconfig']:checked").parent('label').text();
 localStorage.setItem("trayconfig", trayconfig);
  if (localStorage.getItem("trayconfig") === null) {
 $('<p class="summery-text trayconfigtext">TRAY CONFIG: </p><p class="summery-subtext trayconfigsubtext">'+localStorage.getItem("trayconfig")+'</p>').appendTo('#config3');
}
else{
     $(".trayconfigtext").remove();
    $(".trayconfigsubtext").remove();
     $('<p class="summery-text trayconfigtext">TRAY CONFIG: </p><p class="summery-subtext trayconfigsubtext">'+localStorage.getItem("trayconfig")+'</p>').appendTo('#config3');
}
 
});  




//to retrieve depth on 3rd page
$(document).on('click', '.getfixing', function (event) {
$(this).parent().parent().parent().parent().siblings().children().removeClass('highlight',this.checked);
$(this).parent().parent().parent().toggleClass('highlight', this.checked);
	// $(".prevButton").prop("disabled", true);
var traydepth=$("input[name='depth']:checked").parent('label').text();
 localStorage.setItem("traydepth", traydepth);
 if (localStorage.getItem("traydepth") === null) {
     $('<p class="summery-text traydepthtext">TRAY DEPTH: </p><p class="summery-subtext traydepthsubtext">'+localStorage.getItem("traydepth")+'</p>').appendTo('#config3');
  }
   else{
     $(".traydepthtext").remove();
    $(".traydepthsubtext").remove();

 $('<p class="summery-text traydepthtext">TRAY DEPTH: </p><p class="summery-subtext traydepthsubtext">'+localStorage.getItem("traydepth")+'</p>').appendTo('#config3');
}

});  


//to retrieve front fixing on 3rd page
$(document).on('click', '.getotheroptions', function (event) {

var screw=$("input[name='screwplug']:checked").parent('label').text();
 localStorage.setItem("screw", screw);
 if (localStorage.getItem("screw") === null) {
 $('<p class="summery-text trayscrewtext">SCREW : </p><p class="summery-subtext trayscrewsubtext">'+localStorage.getItem("screw")+'</p>').appendTo('#config3');
}
else{
       $(".trayscrewtext").remove();
    $(".trayscrewsubtext").remove();
     $('<p class="summery-text trayscrewtext">SCREW : </p><p class="summery-subtext trayscrewsubtext">'+localStorage.getItem("screw")+'</p>').appendTo('#config3');
}
	
});

//to retrieve other options on 3rd page
$(document).on('click', '.oo', function (event) {
var oo=$("input[name='otheroptions']:checked").parent('label').text();
 localStorage.setItem("oo", oo);
 if (localStorage.getItem("oo") === null) {
 $('<p class="summery-text trayootext">OTHER OPTIONS: </p><p class="summery-subtext trayoosubtext">'+localStorage.getItem("oo")+'</p>').appendTo('#config3');
}
else{
    $(".trayootext").remove();
    $(".trayoosubtext").remove();
    $('<p class="summery-text trayootext">OTHER OPTIONS: </p><p class="summery-subtext trayoosubtext">'+localStorage.getItem("oo")+'</p>').appendTo('#config3');

}

});




//to retrive tray config..soft tip
$(".greenbtnstep3").click(function(e){ 
$("#stepthree").prop("disabled", false);
    $("#trayconfigdiv").empty();
    $(".model3").remove();
    $(".model3subtext").remove();
    $(".color3").remove();
    $(".color3subtext").remove();
    $(".traytype3").remove();
    $(".traytype3subtext").remove();
    $(".trayconfigtext").remove();
    $(".trayconfigsubtext").remove();
    $(".traydepthtext").remove();
    $(".traydepthsubtext").remove();
    $(".trayscrewtext").remove();
    $(".trayscrewsubtext").remove();
    $(".trayootext").remove();
    $(".trayoosubtext").remove();


    $("#traydepthdiv").empty();
    $("#frontfixingdiv").empty();
    $("#otheroptionsdiv").empty();
    $("#getserializeddata").prop("disabled", true);

$('<p class="summery-text model3">MODEL:</p><p class="summery-subtext model3subtext">'+localStorage.getItem("model")+'</p>').appendTo('#config3');
$('<p class="summery-text color3">COLOR:</p><p class="summery-subtext color3subtext">'+localStorage.getItem("color")+'</p>').appendTo('#config3');
$('<p class="summery-text traytype3">TRAY TYPE:</p><p class="summery-subtext traytype3subtext">'+localStorage.getItem("traytype")+'</p>').appendTo('#config3');


var fetchsofttip=$('input[name=traytype]:checked').attr('data-softtip');

  $('#trayconfigloader').show();
    $.ajax( {
        type: "POST",
        url: ajaxurl,
        data: {action:'get_trayconfig',params:fetchsofttip},
        success: function( response ) {

    var obj = JSON.parse(response);

         $('html, body').animate({scrollTop:$('#trayconfigdiv').position().top}, 'slow');
    $("#trayconfigdiv").html(obj.data);
        },
        complete: function(){
 	 $('#trayconfigloader').hide();
    	}
    });

});


//to retrive tray depth based on traytype on click of tray config
$(document).on('click', '.getdepth', function (event) {
$(this).parent().parent().parent().parent().siblings().children().removeClass('highlight',this.checked);
$(this).parent().parent().parent().toggleClass('highlight', this.checked);

var fetchdepth=$('input[name=trayconfig]:checked').attr('data-traydepth');

    $("#traydepthdiv").empty();
    $("#frontfixingdiv").empty();
    $("#otheroptionsdiv").empty();
    $(".traydepthtext").remove();
    $(".traydepthsubtext").remove();
    $(".trayscrewtext").remove();
    $(".trayscrewsubtext").remove();
    $(".trayootext").remove();
    $(".trayoosubtext").remove();
    $("#getserializeddata").prop("disabled", true);



  $('#traydepthloader').show();
    $.ajax( {
        type: "POST",
        url: ajaxurl,
        data: {action:'get_traydepth',params:fetchdepth,formdata:$('#drawerdata').serialize()},
        success: function( response ) {
    var obj = JSON.parse(response);
            // if($('input[name=trayconfig]:checked').is(':checked')) { alert("it's checked"); 
             $('html, body').animate({scrollTop:$('#traydepthdiv').position().top}, 'slow');
    $("#traydepthdiv").html(obj.data);
    // }
    // else{
    //   alert("it's not checked");
    //   $("#traydepthdiv").remove();
    // }
        },
        complete: function(){
 	 $('#traydepthloader').hide();
    	}
    });

});

//to retrive tray fixing type based on traytype on click of tray depth
$(document).on('click', '.getfixing', function (event) {
$("#getserializeddata").prop("disabled", true);
$("#otheroptionsdiv").empty();
$(".trayscrewtext").remove();
$(".trayscrewsubtext").remove();
$(".trayootext").remove();
$(".trayoosubtext").remove();


var fetchfixing=$('input[name=depth]:checked').attr('data-depth');

  $('#frontfixingloader').show();
    $.ajax( {
        type: "POST",
        url: ajaxurl,
        data: {action:'get_fixingtype',params:fetchfixing,formdata:$('#drawerdata').serialize()},
        success: function( response ) {
    var obj = JSON.parse(response);

    $('html, body').animate({scrollTop:$('#frontfixingdiv').position().top}, 'slow');
    $("#frontfixingdiv").html(obj.data);
        },
        complete: function(){
 	 $('#frontfixingloader').hide();
    	}
    });

});

//to retrive tray other options based on traytype on click of tray fixingtype
$(document).on('click', '.getotheroptions', function (event) {
$(this).parent().parent().parent().parent().siblings().children().removeClass('highlight',this.checked);
$(this).parent().parent().parent().toggleClass('highlight', this.checked);
$("#getserializeddata").prop("disabled", false);

var fetchotheroptions=$('input[name=screwplug]:checked').attr('data-screwplug');

$('#otheroptionsloader').show();
    $.ajax( {
        type: "POST",
        url: ajaxurl,
        data: {action:'get_otheroptions',params:fetchotheroptions,formdata:$('#drawerdata').serialize()},
        success: function( response ) {

    var obj = JSON.parse(response);
    
	 $('html, body').animate({scrollTop:$('#otheroptionsdiv').position().top}, 'slow');
    $("#otheroptionsdiv").html(obj.data);
        },
        complete: function(){
 	 $('#otheroptionsloader').hide();
    	}
    });

});



//Order now button
$("#orderbutton").click(function(event){
  $("#orderbutton").prop("disabled", true);
  $("#orderbutton").css("cursor", "default");
  $("#stepone").prop("disabled",true);
  $("#steptwo").prop("disabled",true);
  $("#stepthree").prop("disabled",true);
// alert("Ordered");

 $('#orderedloader').show();
$.ajax( {
        type: "POST",
        url: ajaxurl,
        data: {action:'createorder',params:$('#drawerdata').serialize()},
        success: function( response ) {
          // alert('success');

var obj = JSON.parse(response);


// alert(obj.result_table);
window.location="http://127.0.0.1/drawerconfig/?page_id=700";
// $("#orderdata").html(obj.result_table);

},
        complete: function(){
 	 $('#orderedloader').hide();
    	}
    });
});


$("#getserializeddata").click(function(event){

$('#orderdataloader').show();

$.ajax( {
        type: "POST",
        url: ajaxurl,
        data: {action:'displayorder',params:$('#drawerdata').serialize()},
        success: function( response ) {
var obj = JSON.parse(response);
// alert(obj.result_table);
$("#orderdata").html(obj.result_table);

},
        complete: function(){
   $('#orderdataloader').hide();
      }
    });
});



}