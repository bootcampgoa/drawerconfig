<?php  
/** 
 * Template Name: loginpage 
 */  
 
 get_header();

?>

<?php
if (isset($_SESSION["username"]))
    {
        session_start();
    }

?>

 <link rel="stylesheet" href="<?php bloginfo('template_directory');?>/css/stylelogin.css"> 

  <div class="form">
      
      <ul class="tab-group">
        <li class="tab active"><a href="#signup">Sign Up</a></li>
        <li class="tab"><a href="#login">Log In</a></li>
      </ul>
      
      <div class="tab-content">
        <div id="signup">   
          <h1 class="signuplogoin">Sign Up for Free</h1>
          
          <form onsubmit="return false" method="post" id="registerform" autocomplete="off">
          <div class="hidden">
            <input type="password"/>
          </div>
         
            <div class="field-wrap">
              <label>
                Username<span class="req">*</span>
              </label>
              <input id="uname" type="text" name="username" readonly onfocus="$(this).removeAttr('readonly');" required autocomplete="off"/>
            </div>
         

          <div class="field-wrap">
            <label>
              Email Address<span class="req">*</span>
            </label>
            <input id="email" type="email" name="email" readonly onfocus="$(this).removeAttr('readonly');" required autocomplete="off"/>
          </div>
          
          <div class="field-wrap">
            <label>
              Set A Password<span class="req">*</span>
            </label>
            <input id="password" type="password" name="pass" readonly onfocus="$(this).removeAttr('readonly');" required autocomplete="off"/>
          </div>
          
          <button class="button button-block" id="submitbutton" />Get Started</button>
           <div class="field-wrap"></div>
            <div class="field-wrap" id="showregistermsg">


            </div>
          </form>
          <?php //echo "count-".$count; ?>

        </div>
        
        <div id="login">   
          <h1 class="signuplogoin">Welcome Back!</h1>
          
          <form onsubmit="return false" method="post" id="loginform" autocomplete="off">
          
            <div class="field-wrap">
            <label>
              Username<span class="req">*</span>
            </label>
            <input id="loginuname" type="text" name="username" readonly onfocus="$(this).removeAttr('readonly');" required autocomplete="off"/>
          </div>
          
          <div class="field-wrap">
            <label>
              Password<span class="req">*</span>
            </label>
            <input id="loginpass" type="password" name="pass" readonly onfocus="$(this).removeAttr('readonly');" required autocomplete="off"/>
          </div>
          
          <!-- <p class="forgot"><a href="#">Forgot Password?</a></p> -->
          
          <button class="button button-block" id="loginbutton" />Log In</button>
            <div class="field-wrap"></div>
            <div class="field-wrap" id="showloginmsg">


            </div>
          </form>

        </div>
        
      </div>
      
</div> 


<?php get_footer(); ?>
