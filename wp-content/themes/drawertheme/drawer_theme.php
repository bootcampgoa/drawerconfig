<?php /* Template Name: drawer */ ?>
<?php 
 echo
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-Type: text/html');
session_start();
if (!isset($_SESSION["username"]))
   {
       header("location: http://127.0.0.1/drawerconfig/?page_id=681");
       exit;
   }

ini_set('display_errors',1); 
error_reporting(E_ALL); 
    // echo "username";


// $_SESSION['username']=$username;
get_header();
?>

<body>


<!-- main content -->
<section class="form-box">
    <div class="form-wizard">

        <!-- Form Wizard -->
        <form role="form" onsubmit="return false" method="get" id="drawerdata">
            <header class="site-header">
                <div class="container">
                    <div class="row">
                        <div class="site-header-wrapper col-sm-12">
                            <div class="site-header-wrap col-sm-4">
                                <h3><a class="webtitle" href="<?php echo $home_url; ?>">DELTAMAC DRAWER SYSTEM</a></h3>
                            </div>
                            <!--  <p>Fill all form field to go next step</p -->

                            <!-- Form progress -->
                            <div class="site-header-wrap col-sm-6">
                                <div class="form-wizard-steps form-wizard-tolal-steps-4">
                                    <div class="form-wizard-progress">
                                        <div class="form-wizard-progress-line" data-now-value="31.25" data-number-of-steps="3" style="width: 31.25%;"></div>
                                    </div>
                                    <!-- Step 1 -->
                                    <div class="form-wizard-step active" id="fws1">
                                        <!-- <div class="form-wizard-step-icon" id="stepone">
                                        </div> -->
                                         <button type="button" class="form-wizard-step-icon" id="stepone"></button>

                                        <!-- <p>Step 1</p> -->
                                        <p>Model Series</p>
                                    </div>

                                    <!-- Step 1 -->

                                    <!-- Step 2 -->
                                    <div class="form-wizard-step" id="fws2">
                                       <!--  <div class="form-wizard-step-icon" id="steptwo">
                                       
                                        </div> -->
                                           <button type="button" class="form-wizard-step-icon" id="steptwo"   disabled="disabled"></button>
                                        <!-- <p>Step 2</p> -->
                                        <p>Tray Type</p>
                                    </div>
                                    <!-- Step 2 -->

                                    <!-- Step 3 -->
                                    <div class="form-wizard-step" id="fws3">
                                        <!-- <div class="form-wizard-step-icon"  id="stepthree">
                                        </div> -->
                                         <button type="button" class="form-wizard-step-icon" id="stepthree"   disabled="disabled"></button>
                                        <!-- <p>Step 3</p> -->
                                        <p>Variable</p>
                                    </div>
                                    <!-- Step 3 -->

                                    <!-- Step 4 -->
                                    <div class="form-wizard-step" id="fws4">
                                        <div class="form-wizard-step-icon" id="stepfour">
                                        <!-- <i class="fa fa-check" aria-hidden="true"></i> -->
                                       <!--  4 -->
                                        </div>
                                        <!-- <p>Step 4</p> -->
                                        <p>Review</p>
                                    </div>
                                    <!-- Step 4 -->
                                </div>
                            </div>
                            <!-- Form progress -->
                             <div class="site-header-wrap col-sm-2"><span class="h4" id="userhello">Hello <?php 

                             echo $_SESSION['username']; 
                             // session_destroy(); ?></span> <a href="#" id="logout"><i class="fa fa-power-off fa-2x" aria-hidden="true"></i>
</a></div>
                        </div>
                    </div>
                </div>

            </header>



        <?php


        $args = array
        (
        'post_type' => 'product', // it's default, you can skip it
        'posts_per_page' => '3',
        'order_by' => 'date', // it's also default
        'order' => 'ASC', // it's also default
        'tax_query' => array(
        array(
        'taxonomy' => 'product_cat',
        'terms' => 'mainproducts',
        'field' => 'slug',
        )
        )
        );
        $query = get_posts( $args );


        ?>
            <div class="step-section">
   
          
                    <!-- Form Step 1 -->
                    <fieldset id="steponefields">

                        <div class="colorwhite min-height">
                            <div class="container">
                                <div class="row">
                                <div class="col-sm-12">
                                  <div class="modelseriestext"><h1 >Select your model series</h1></div>
                                </div>
                                 <?php
                                    // print_r($_SESSION); ?>
                                
                                <!-- //logic to show first two prducts -->
                            <div class="col-sm-12 col-md-9">

                                <div class='step-section-spacer col-sm-12 col-md-9 no-pad'>
                                   
                                <?php
                                foreach ($query as $value) {

                                $id=$value->ID;
                                $name=$value->post_title;
                                // echo "<div class='col-sm-6 col-md-4'";
                                // echo "</div>";
                                echo "<div class='col-sm-6 col-md-6'>";
                                echo "<div class='product-grid' id='green'>";
                                if (has_post_thumbnail($id) ):
                                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $id), 'single-post-thumbnail' );
                                echo "<img src='".$image[0]."'>";  
                                endif;
                                echo "<div class='radio'>";
                                echo "<label><input type='radio' id='". $id."' name='model' value='". $id."' class='product'><span></span>".$name;
                                echo "</label>";
                                echo "</div>";
                                echo "</div>";
                                echo "</div>";
                                }
                                
                                ?>

                                </div>

                                

                                <?php 
                                ?>
                               
                                <div id="colordiv" class="step-section-spacer col-sm-12 col-md-9 no-pad">
                                    <img src="http://bootcampgoa.kwrk.in/drawer/wp-content/uploads/2017/04/loading-image.gif" id="colorload" width="50px" height="50px" style="display:none;">

                                    <?php


                                    ?>
                                </div>

                               </div>
                               <div class="col-sm-12 col-md-3 summery-box" id="summarypage1">
                                        <p class="summery-title"><img class="summery-icon" src="<?php echo $cdn_url; ?>wp-content/themes/drawertheme/images/list@2x.png"> Summary</p>
                                        <div class="col-sm-12 infodiv" id="pageoneconf">



                                        </div>
                                </div>
                             

                            

                                </div>
                            </div>
                        </div> 
                        <div class="form-wizard-buttons col-sm-12">
                            <div class="container">
                                <div class="row">
                                    <div class="col-xs-4 no-pad"></div>
                                    <div class="col-xs-8 no-pad"><button type="button" class="btn btn-next nextButton greenbtnstep1"  disabled="disabled">NEXT</button></div>
                                </div>
                            </div>
                        </div>
                        <!-- </div>colorwhite end -->
                    </fieldset>

                    <!-- Form Step 1 -->




                    <!-- Form Step 2 -->
                    <fieldset id="steptwofields">

                        <div class="colorwhite min-height">
                        <div class="container">
                            <div class="row">
                                <div class="col-sm-12">
                                     <div class="modelseriestext"><h1>Select your type of tray</h1></div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="col-sm-12 col-md-9 no-pad" id="traydiv">
                                        <img src="http://bootcampgoa.kwrk.in/drawer/wp-content/uploads/2017/04/loading-image.gif" id="trayload" width="50px" height="50px" style="display:none;">
                                        <?php

                                        ?>
                                    </div>
                                    <div class="col-sm-12 col-md-3 summery-box">
                                        <p class="summery-title"><img class="summery-icon" src="<?php echo $cdn_url; ?>wp-content/themes/drawertheme/images/list@2x.png"> Summary</p>
                                        <div class="col-sm-12 infodiv" id="conf">



                                        </div>
                                    </div>
                                </div>
                            </div><!--row close-->
                        </div>


                        <div class="form-wizard-buttons col-sm-12">
                            <div class="container">
                                <div class="row">
                                    <div class="col-xs-6 no-pad"><button type="button" class="btn btn-previous prevButton" id="prevbuttontwo">PREV</button></div>
                                    <div class="col-xs-6 no-pad"><button type="button" class="btn btn-next nextButton greenbtnstep3" disabled="disabled" >NEXT</button></div>
                                </div>
                            </div>
                        </div>

                        <!-- </div>colorwhite end -->
                        
                    </fieldset>

                    <!-- Form Step 2 -->




                    <!-- Form Step 3 -->
                    <fieldset id="stepthreefields">

                        <div class="colorwhite min-height">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-12">
                                         <div class="configurationtext"><h1>Configure your drawer</h1></div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="col-sm-12 col-md-9 no-pad">
                                            <div class="col-sm-12" id="trayconfigdiv">
                                                <img src="http://bootcampgoa.kwrk.in/drawer/wp-content/uploads/2017/04/loading-image.gif" id="trayconfigloader" width="50px" height="50px" style="display:none;">

                                            </div>

                                            <div id="traydepthdiv" class="col-sm-12">
                                                <div class="col-sm-12">
                                                 <img src="http://bootcampgoa.kwrk.in/drawer/wp-content/uploads/2017/04/loading-image.gif" id="traydepthloader" width="50px" height="50px" style="display:none;">
                                                </div>

                                            </div>

                                            <div id="frontfixingdiv" class="col-sm-12">
                                                <div class="col-sm-12">
                                                    <img src="http://bootcampgoa.kwrk.in/drawer/wp-content/uploads/2017/04/loading-image.gif" id="frontfixingloader" width="50px" height="50px" style="display:none;">
                                                </div>


                                            </div>

                                            <div class="col-sm-12" id="otheroptionsdiv">
                                                <div class="col-sm-12">
                                                <img src="http://bootcampgoa.kwrk.in/drawer/wp-content/uploads/2017/04/loading-image.gif" id="otheroptionsloader" width="50px" height="50px" style="display:none;">
                                                </div>
                                                <?php


                                                ?>

                                            </div>
                                        </div>

                                        <div class="col-sm-12 col-md-3 summery-box">
                                            <p class="summery-title"><img class="summery-icon" src="<?php echo $cdn_url; ?>wp-content/themes/drawertheme/images/list@2x.png">Summary</p>
                                            <div class="col-sm-12 infodiv" id="config3">



                                            </div>
                                        </div>  
                                    </div>
                                </p>
                            </div>
                        </div><!-- container end -->        
                  
                        <div class="form-wizard-buttons col-sm-12">
                            <div class="container">
                                <div class="row">
                                    <div class="col-xs-6 no-pad">
                                        <button type="button" class="btn btn-previous prevButton">PREV</button>
                                    </div>
                                    <div class="col-xs-6 no-pad">
                                        <button type="button" class="btn btn-next nextButton" id="getserializeddata"   disabled="disabled">NEXT</button>
                                    </div>
                                </div>
                            </div>
                        </div>



                    </fieldset>

                    <!-- Form Step 3 -->

                    <!-- Form Step 4 -->
                    <fieldset id="stepfourfields">
                        
                        <div class="colorwhite min-height">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h1>Summary of your configuration</h1>
                                    </div>
                                   
                                    
                                    <div class="col-sm-12" id="orderdata">
                                        <img src="http://bootcampgoa.kwrk.in/drawer/wp-content/uploads/2017/04/loading-image.gif" id="orderdataloader" width="50px" height="50px" style="display:none;">

                                    </div>
                                   <div class="loadingdiv" style="position:absolute;left:50%;top:50%;transform:translate(-50%,-50%);"><img src="http://127.0.0.1/drawerconfig/wp-content/uploads/2017/04/loading-image.gif" id="orderedloader" width="100px" height="100px" style="display:none;"></div>
                                </div>
                            </div>
                         
                            <div class="form-wizard-buttons">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <button type="button" class="btn btn-primary orderButton" id="orderbutton">Order Now</button>

                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>
                   
                    </fieldset>
                    <!-- Form Step 4 -->
               
                 
            </div>

        </form>
    <!-- Form Wizard -->
    </div>
</section>
<!-- main content -->


<?php get_footer(); ?>
