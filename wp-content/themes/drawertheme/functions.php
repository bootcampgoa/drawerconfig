<?php

register_nav_menus( array( 
        'header' => 'Header menu', 
        'footer' => 'Footer menu' 
      ) );

if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name' => 'Sidebar Widgets',
        'id'   => 'sidebar-widgets',
        'description'   => 'These are widgets for the sidebar.',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ));
}


print apply_filters('taxonomy-images-queried-term-image-url', '' );

add_action( 'wp_ajax_nopriv_get_colors', 'getproductcolors' );
add_action( 'wp_ajax_get_colors', 'getproductcolors' );

function getproductcolors()
{

  $result=$_POST['params'];
  $colordata="";

  if($result['model'])
  {
 $colordata.="<div class='col-sm-12'><h1>Choose Color</h1></div>";
    $product_terms = wp_get_object_terms($result,'pa_color');
        foreach ($product_terms as $key ) 
        {

            $color=$key->name;
            $colorid=$key->term_id;
            $terms = apply_filters( 'taxonomy-images-get-terms', '', array(
                'taxonomy' => 'pa_color',
               
            ) );
            if ( ! empty( $terms )) {
                
                foreach( (array) $terms as $term )
                 {
                        $termid=$term->term_id;
                    
                     if($termid==$colorid)
                     {
                         $colordata.='<div class="col-sm-6 col-md-6"> <div class="product-grid">';
                         
                       $colordata.= wp_get_attachment_image( $term->image_id, "full", "", array( "class" => "img-responsive imageclass" ));
                       // print_r($colordata);
                       // exit();
                         
                        $colordata.="<div class='radio'>";
                       $colordata.="<label><input type='radio' name='color' id='". $colorid."' value='".$colorid."' data-trayid='".$result."' class='colors'><span></span>".$color;
                           $colordata.="</label>";
                            $colordata.="</div>";
                           $colordata.='</div></div>';
                     }
                  }
                
            }

        }
  }
  
 echo json_encode(array('data'=>$colordata));die();
}


add_action( 'wp_ajax_nopriv_get_traytype', 'gettraytype' );
add_action( 'wp_ajax_get_traytype', 'gettraytype' );

function gettraytype()
{

$traydata="";


  if($_POST['params'])
  {
  
  
  if($_POST['params']==322) { 
    $category= array('66','67');
    
  }
  else  
  {
    $category=array('76','81','82','83');   
  }
  foreach($category as $cat=>$value){
  
      $a=get_posts(array(
    'showposts' => -1,
    'post_type' => 'product',
    'tax_query' => array(
        array(
        'taxonomy' => 'product_cat',
        'field' => 'term_id',
        'terms' => $value)
    ),
    'orderby' => 'title',
    'order' => 'ASC')
);

$image=get_category_images($value);

$traydata.='<div class="col-sm-6 col-md-4"><div class="product-grid"><img src="'.$image.'">';
foreach($a as $key=>$val){
  
                 $product = wc_get_product( $val);
                 $traydata.="<div class='radio'>";
                 $traydata.= "<label><input type='radio' name='traytype' class='traytype' 
                 value='".$product->get_ID()."' data-softtip='".$product->get_ID()."' 
                 selected ><span></span><span class='aligntext'>".$product->get_title();
                   $traydata.= "</span></label></div>";
}
 $traydata.='</div></div>';
}
 }

  echo json_encode(array('data'=>$traydata));die();
}

add_action( 'wp_ajax_nopriv_get_trayconfig', 'gettrayconfig' );
add_action( 'wp_ajax_get_trayconfig', 'gettrayconfig' );

function gettrayconfig()
{  

$trayconfigdata="";

    if( $_POST['params'])
  {                   

             $trayconfig=$_POST['params'];
            
              $key_1_value = get_post_meta(  $_POST['params'], 'linkproducts', true );
              // Check if the custom field has a value.
              if ( ! empty( $key_1_value ) ) {
                  foreach ($key_1_value as $key=>$value ) {
                    $product = wc_get_product( $value );
                    
                }
                foreach ($key_1_value as $key ) {
                 
                  
                   $product = wc_get_product( $key );
                   
                     $term_list=wp_get_post_terms($product->get_ID(), 'product_cat',  array("fields" => "ids"));
                                
                 $a=get_category_images($term_list[0]);
              
                   
                   $trayconfigdata.="<div class='col-sm-6 col-md-4'><div class='product-grid'>";
                      $trayconfigdata.="<img src='".$a."'>";
                            
                   $trayconfigdata.="<div class='radio'>";
                             
                   $trayconfigdata.= "<label><input type='radio' name='trayconfig' class='getdepth' 
                   value='".$product->get_ID()."' data-traydepth='".$product->get_ID()."'><span></span><span class='aligntext'>".$product->get_title();
                            
                   $trayconfigdata.= "</span></label></div></div></div>";
                }
          }
    }
  
    echo json_encode(array('data'=>$trayconfigdata));die();
}


add_action( 'wp_ajax_nopriv_get_traydepth','gettraydepth');
add_action( 'wp_ajax_get_traydepth', 'gettraydepth' );

function gettraydepth()
{

parse_str ( $_POST['formdata'],$result);

$trayconfigdepth="";

if( $_POST['params'])
  {   
 
            $key_1_value = get_post_meta( $_POST['params'], 'depth', true );
            // Check if the custom field has a value.
            if ( ! empty( $key_1_value ) ) {
            
            
              foreach ($key_1_value as $key=>$value ) {
                    $product = wc_get_product( $value );

                }
                $term_list=wp_get_post_terms($product->get_ID(), 'product_cat',  array("fields" => "ids"));
                
             
                
              $a=get_category_images($term_list[0]);
             
              $trayconfigdepth.= "<div class='col-sm-12'><h1>Tray Depth</h1></div><div class='col-sm-6 col-md-4'><div class='product-grid'><img src='".$a."'>";
              foreach ($key_1_value as $key ) {

                 $product = wc_get_product( $key );
                 
            $trayconfigdepth.=  "<div class='radio'>";

              $trayconfigdepth.=  "<label><input type='radio' class='getfixing' name='depth' data-depth='".$result['traytype']."'
             value='".$product->get_ID()."'><span></span>".$product->get_title();
              $trayconfigdepth.=  "</label></div>";
          }
            $trayconfigdepth.="</div></div>";
        }
     }
   echo json_encode(array('data'=>$trayconfigdepth));die();
  }






add_action( 'wp_ajax_nopriv_get_fixingtype','getfixingtype');
add_action( 'wp_ajax_get_fixingtype', 'getfixingtype' );



function getfixingtype()
{
parse_str ( $_POST['formdata'],$result);

$screwplug="";
if( $_POST['params'])

  {    


  
            $key_1_value = get_post_meta($_POST['params'], 'frontfixing', true );
           
            // Check if the custom field has a value.

            if ( ! empty( $key_1_value ) ) {
               
              $screwplug.= "<div class='col-sm-12'><h1>FRONT FIXING</h1></div>";
              foreach ($key_1_value as $key ) {
                
                 $product = wc_get_product( $key );
                 
                  $term_list=wp_get_post_terms($product->get_ID(), 'product_cat',  array("fields" => "ids"));
                                
                 $a=get_category_images($term_list[0]);
              
                   
                   $screwplug.="<div class='col-sm-6 col-md-4'><div class='product-grid'>";
                      $screwplug.="<img src='".$a."'>";
                      
                      
                 
           $screwplug.= "<div class='radio'>";
                 $screwplug.= "<label><input type='radio' class='getotheroptions' name='screwplug' data-screwplug='".$result['traytype']."' value='".$product->get_ID()."'><span></span>".$product->get_title();
              $screwplug.="</label></div></div></div>";
            
          }
        }
       
  }
   echo json_encode(array('data'=>$screwplug));die();
}







add_action( 'wp_ajax_nopriv_get_otheroptions','getotheroptions');
add_action( 'wp_ajax_get_otheroptions', 'getotheroptions' );



function getotheroptions()
{

parse_str ( $_POST['formdata'],$result);
$otheroptions="";
  // if($result['otheroptions'])
    if( $_POST['params'])
  {    


            $key_1_value = get_post_meta($_POST['params'], 'otheroptions', true );
          
            // Check if the custom field has a value.
            if ( ! empty( $key_1_value ) ) {
            
               $otheroptions.="<div class='col-sm-12'><h1>OTHER OPTIONS</h1></div>";
              foreach ($key_1_value as $key ) {
                
                 $product = wc_get_product( $key );
                 
                 
                  $term_list=wp_get_post_terms($product->get_ID(), 'product_cat',  array("fields" => "ids"));
                                
                 $a=get_category_images($term_list[0]);
              
                   
                   $otheroptions.="<div class='col-sm-6 col-md-4'><div class='product-grid'>";
                      $otheroptions.="<img src='".$a."'>";
                 
                 
                 
                 
           $otheroptions.= "<div class='radio'>";
              $otheroptions.=  "<label><input type='radio' class='oo' name='otheroptions' value='".$product->get_ID()."'><span></span>".$product->get_title();
           $otheroptions.=  "</label></div></div></div>";
          
           
          }
          
        }
        
  }
  echo json_encode(array('data'=>$otheroptions));die();
}



//valid email
// function isEmail(email) {
//   var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
//   return regex.test($email);
// }


add_action( 'wp_ajax_nopriv_get_registeruser','getregisteruser');
add_action( 'wp_ajax_get_registeruser', 'getregisteruser' );



function getregisteruser()
{
    // echo "<html><body><script>alert('inside');</script></body></html>";

    parse_str ( $_POST['formdata'],$res);

    $username = $res['username'];
    $email = $res['email'];
    $pass = $res['pass'];
    // echo "un".$username;
    // exit();
    // wp_create_user( $username, $pass, $email );

// $fillfields="";
if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
  $emailErr = 0; 
}
else{
  $emailErr=1;
}
    $emailexists=email_exists($email);
    $user_id = username_exists( $username );
    // echo "vbll";
    $msg="";
    if ( $user_id == false && $emailexists==false && $emailErr==1 && $pass!="" && $username!="") {
      wp_create_user( $username, $pass, $email );
      $msg.="<div class='alert alert-success' id='success'> <strong>Username Registered Successfully! Login to continue</strong></div>";
      //print_r($msg);
      $userexists=0;
    } elseif($user_id == true) {
      $userexists=1;
      $msg="<div class='alert alert-danger' id='fail'><strong>Username Already Exists! Please Enter a Different Username</strong></div>";
      // print_r($msg);
       $fillfields=20;
    }
    elseif($emailexists == true) {
      $msg="<div class='alert alert-danger' id='fail'><strong>Email id Already Exists! Please Enter a Different Email Id</strong></div>";
      $fillfields=5;
      // print_r($msg);
    }
     elseif($username=="" && $emailErr==0 && $pass==""){
      $fillfields=1;
      $msg="<div class='alert alert-danger' id='fail'><strong>Please fill in all the fields</strong></div>";
    }

    elseif($emailErr==0 && $email!=""){
       $userexists=2;
       $fillfields=5;
      $msg="<div class='alert alert-danger' id='fail'><strong>Invalid Email id</strong></div>";
    }
    elseif($pass==""){
       $fillfields=25;
       $msg="<div class='alert alert-danger' id='fail'><strong>Please fill in all the fields</strong></div>";
    }
      elseif($username=="" || $email=="" || $pass==""){
      // $fillfields=1;
      $msg="<div class='alert alert-danger' id='fail'><strong>Please fill in all the fields</strong></div>";
      
    }
    
   

    // elseif($pass==""){
    //    $msg="<div class='alert alert-danger' id='fail'><strong>Please fill in the password field</strong></div>";
    // }

    


   // print_r($msg);

    echo json_encode(array('data'=>$msg,'userexists'=>$userexists,'fill'=>$fillfields));die();
}





add_action( 'wp_ajax_nopriv_get_login','getlogin');
add_action( 'wp_ajax_get_login', 'getlogin' );



function getlogin()
{
    parse_str ( $_POST['formdata'],$res);
    $username = $res['username'];
    $pass = $res['pass'];

    $msg="";
    $user = get_user_by( 'login', $username );
    $userID=$user->ID;
 
    $user_info = get_userdata($user_id);
    $userEmail = $user_info->user_email;
    $all_meta_for_user = get_userdata($userID );
    $userEmail=$all_meta_for_user->user_email;
    $userNickname=$all_meta_for_user->user_nicename;
    // print_r( $all_meta_for_user );
    // print_r($user_info);
    
    if ( $user && wp_check_password( $pass, $user->data->user_pass, $user->ID) )
    {
      // $msg="That's it";
      session_start();
      $_SESSION['username']=session_id();
      $msg='http://127.0.0.1/drawerconfig/?page_id=325';
      $status=0;
     
      $_SESSION['username']=$username;
      $_SESSION['userId']=$userID;
      // $_SESSION['userfirstname']=$first_name;
      $_SESSION['email']=$userEmail;
      $_SESSION['nickname']=$userNickname;
     }
    elseif($username=="" || $pass==""){
        $fillfields=1;
      $msg="<div class='alert alert-danger' id='fail'><strong>Please fill in the details to log in</strong></div>";;
      // $status=1;
    }

    else{
       $msg="<div class='alert alert-danger' id='fail'><strong>Invalid Username or Password</strong></div>";;
      $status=1;
       $fillfields=10;
    }
    echo json_encode(array('data'=>$msg,'status'=>$status,'username'=>$username,'fill'=>$fillfields));die();
    // echo json_encode(array('data'=>$msg));die();
}

add_action( 'wp_ajax_nopriv_get_logout','getlogout');
add_action( 'wp_ajax_get_logout', 'getlogout' );



function getlogout()
{
  session_start();
  session_destroy();
  setcookie(PHPSESSID,session_id(),time()-1);

     $msg='http://127.0.0.1/drawerconfig/?page_id=681';
    echo json_encode(array('data'=>$msg));die();
    // echo json_encode(array('data'=>$msg));die();
}




//to get images
function get_category_images($id){

    global $wpdb;
    
    $mylink = $wpdb->get_row( "SELECT meta_value FROM wp_termmeta WHERE term_id ='".$id."' and meta_key='thumbnail_id'" );
    
    $image = wp_get_attachment_url( $mylink->meta_value );
      
  
  return $image;
    
    
    }
    
// Triggers for this email 

add_action( 'woocommerce_order_status_wc-pending_to_wc-processing_notification', array( $this, 'trigger' ) );
    
 





// display order data
add_action( 'wp_ajax_nopriv_displayorder','displayorder');
add_action( 'wp_ajax_displayorder', 'displayorder' );
function displayorder()
{
error_reporting(1); 
  parse_str($_POST['params'],$results);


// print_r($results);
// exit();
 // global $woocommerce;
 //    $woo = $woocommerce->cart->get_cart();
 //    print_r($woo);
    

   
 // $product = wc_get_product( 339 );
// echo $product->get_title();
// echo $product->get_price();
// exit();


                
$row.="<table class='table table-striped'> <thead>
                                      <tr>
                                        <th>PRODUCT NAME</th>
                                        
                                        <th>PRODUCT COST</th>
                                        <th></th>
                                      </tr>
                                    </thead>";
                                    $grandtotal="";

                                 $row.=  "<tr><td>configurator</td><td></td><td></td></tr>";
//  $product = wc_get_product( 31 );
// echo $product->get_title();
// echo $product->get_price();
 // $row.="<tr><td><h4>". $product->get_title()."</h4></td>";
 //  $row.="<td><h4>NOK ".$product->get_price()."</h4></td><td></td></tr>";
// print_r($results);
// exit();
$value="";
foreach ( $results as $item=>$value ) {

    if($item!="color"){
     $product = wc_get_product( $value );
    
    // $product_name = $product->get_title();
    // echo $product_name;
   
    $row.="<tr><td><h4>".$product->get_title()."</h4></td>";
      // echo $product->get_title();
     // $row.=$product->get_title();
   
    // $product_qty = $item['qty'];

    // $product_cost = $product->get_price();
    // echo $product_cost;
   
    // $grandtotal=$grandtotal+$product_cost ;
      $row.="<td><h4>NOK ".$product->get_price()."</h4></td><td></td></tr>";
      $grandtotal=$grandtotal+$product->get_price();
      // echo $product->get_price();
     // $row.=$product->get_price();

      }


    
}

 
// $row.="</table></div></div></div>";
 
$row.="<tr><td><h3 class='weight'>TOTAL</h3></td><td><h3 class='weight'>NOK ".$grandtotal."</h3></td><td></td></tr></table>";

// $row.="<form id='hiddenform' method='post' >";
// foreach ( $results as $item=>$value ) {
//   $product = wc_get_product( $value );
// $row.="<input type='hidden' name='".$product->get_title()."' id='".$value."'>";

// }

// // $row.="<input type='hidden' name='".$product->get_title()."' id='".$value."'>";
// $row.="</form>";



echo json_encode(array('result_table'=>$row));die();



}





//create order code starts
add_action( 'wp_ajax_nopriv_createorder','createorder');
add_action( 'wp_ajax_createorder', 'createorder' );


function createorder()
{
error_reporting(1); 
  parse_str($_POST['params'],$results);

  // print_r($results);
  // exit();
 session_start();
$address = array(
            'first_name' => $_SESSION['nickname'],
            'last_name'  => 'test lastname',
            'company'    => 'testcompany',
            'email'      => $_SESSION['email'],
            'phone'      => '777-777-777-777',
            'address_1'  => 'Test address 1',
            'address_2'  => '', 
            'city'       => 'Test City',
            'state'      => 'Test State',
            'postcode'   => '12345',
            'country'    => 'IN'
        );

// print_r($address);
// exit();

$order=wc_create_order();
$order->set_address( $address, 'billing' );
$order->set_address( $address, 'shipping' );
$total="";
foreach ($results as $key=>$value) {
  $regularprice=100;

if($key!= "color"){

  $product_to_add = wc_get_product($value);
$price = $product_to_add->get_price();

$salesprice=$product_to_add->get_sale_price();


$total=$total+$price;



$price_params = array( 'totals' => array('total'=>$total ) );//2=qty
$order->add_product( get_product($value), 1, $price_params );
}
}
$order->set_total($total);


$order = new WC_Order( $order->id );
$order->update_status('processing');
$items = $order->get_items();
                
// $row="<div class='container'><div class='row'>
$row="<table class='table table-striped'> <thead>
                                      <tr>
                                        <th>PRODUCT NAME</th>
                                        
                                        <th>PRODUCT COST</th>
                                        <th></th>
                                      </tr>
                                    </thead>";
                                    $grandtotal="";

                                 $row.=  "<tr><td>configurator</td><td></td><td></td></tr>";
foreach ( $items as $item ) {
 
 
    $product_name = $item['name'];
   
    $row.="<tr><td><h4>".$product_name."</h4></td>";
    
    $product_qty = $item['qty'];

    $product_cost = $item['line_subtotal'];
    
    $grandtotal=$grandtotal+$product_cost ;
      $row.="<td><h4>NOK ".$product_cost."</h4></td><td></td></tr>";

      

    
}
// $row.="<tr><td><h3 class='weight'>TOTAL</h3></td><td><h3 class='weight'>NOK ".$grandtotal."</h3></td><td></td></tr></table></div></div></div>";
$row.="<tr><td><h3 class='weight'>TOTAL</h3></td><td><h3 class='weight'>NOK ".$grandtotal."</h3></td><td></td></tr></table>";

$row.="<form id='hiddenform' method='post' >";
foreach ( $items as $item ) {
$row.="<input type='hidden' name='".$item['name']."' id='".$item['product_id']."'>";

}

$row.="<input type='hidden' name='".$item['name']."' id='".$order->id."'>";
$row.="</form>";



echo json_encode(array('result_table'=>$row));die();



}





?>




