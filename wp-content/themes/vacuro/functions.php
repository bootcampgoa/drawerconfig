
<?php

register_nav_menus( array( 
        'header' => 'Header menu', 
        'footer' => 'Footer menu' 
      ) );

if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name' => 'Sidebar Widgets',
        'id'   => 'sidebar-widgets',
        'description'   => 'These are widgets for the sidebar.',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h2>',
        'after_title'   => '</h2>'
    ));
}
// Our custom post type function
function create_posttype() {
//registers a post type called Orders 
	//second argument is array..which has two parts..first array takes labels and name and second takes oder para like public ,has archive
	register_post_type( 'orders',
	// CPT Options
		array(
			'labels' => array(
				'name' => __( 'Orders' ),
				'singular_name' => __( 'Order' )
			),
			'description'=> __( 'order types take away and have here', 'customtemplate' ),
			'public' => true,
			'has_archive' => true,
			'rewrite' => array('slug' => 'orders'),
      'taxonomies' => array('post_tag'),
      'taxonomies' => array('topics', 'category' ),
		)
	);
//***************create a custom  post type for food item ////


//registers a post type called Orders 
  //second argument is array..which has two parts..first array takes labels and name and second takes oder para like public ,has archive
  register_post_type( 'fooditem',
  // CPT Options
    array(
      'labels' => array(
        'name' => __( 'fooditem' ),
        'singular_name' => __( 'fooditem' )
      ),
      'description'=> __( 'food item categorised on type and price', 'customtemplate' ),
      'public' => true,
      'has_archive' => true,
      'rewrite' => array('slug' => 'fooditem'),
     
    )
  );
}
// Hooking up our function to theme setup
add_action( 'init', 'create_posttype' );










/**
 * Add custom taxonomies
 *
 * Additional custom taxonomies can be defined here
 * http://codex.wordpress.org/Function_Reference/register_taxonomy
 */
function add_custom_taxonomies() {
  // Add new "Locations" taxonomy to Posts
  register_taxonomy('take away', 'orders', array(
    // Hierarchical taxonomy (like categories)
    'hierarchical' => true,
    // This array of options controls the labels displayed in the WordPress Admin UI
    'labels' => array(
      'name' => _x( 'take away orders', 'take away order type' ),
      'singular_name' => _x( 'take away order', 'take away order' ),
      'search_items' =>  __( 'Search Ordrers' ),
      'all_items' => __( 'All Orders' ),
      'parent_items' => __( 'Orders' ),
      'parent_items_colon' => __( 'Parent Location:' ),
      'edit_items' => __( 'Edit Order' ),
      'update_items' => __( 'Update Order' ),
      'add_new_item' => __( 'Add New Order' ),
      'new_items' => __( 'New Order Name' ),
      'menu_name' => __( 'take away' ),
    ),
    // Control the slugs used for this taxonomy
    'rewrite' => array(
      'slug' => 'takeaway', // This controls the base slug that will display before each term
      'with_front' => false, // Don't display the category base before "/locations/"
      'hierarchical' => true // This will allow URL's like "/locations/boston/cambridge/"
    ),
  ));

	register_taxonomy('have here', 'orders', array(
    // Hierarchical taxonomy (like categories)
    'hierarchical' => true,
    // This array of options controls the labels displayed in the WordPress Admin UI
    'labels' => array(
      'name' => _x( 'have here orders', 'have here order type' ),
      'singular_name' => _x( 'have here order', 'have here order' ),
      'search_items' =>  __( 'Search Ordrers' ),
      'all_items' => __( 'All Orders' ),
      'parent_items' => __( 'Orders' ),
      'parent_items_colon' => __( 'Parent Location:' ),
      'edit_items' => __( 'Edit Order' ),
      'update_items' => __( 'Update Order' ),
      'add_new_item' => __( 'Add New Order' ),
      'new_items' => __( 'New Order Name' ),
      'menu_name' => __( 'have here' ),
    ),
    // Control the slugs used for this taxonomy
    'rewrite' => array(
      'slug' => 'havehere', // This controls the base slug that will display before each term
      'with_front' => false, // Don't display the category base before "/locations/"
      'hierarchical' => true // This will allow URL's like "/locations/boston/cambridge/"
    ),
  ));
register_taxonomy('type', 'fooditem', array(
    // Hierarchical taxonomy (like categories)
    'hierarchical' => true,
    // This array of options controls the labels displayed in the WordPress Admin UI
    'labels' => array(
      'name' => _x( 'foodtype', 'foodtype' ),
      'singular_name' => _x( 'type', 'type' ),
      'search_items' =>  __( 'Search Ordrers' ),
      'all_items' => __( 'type' ),
      'parent_items' => __( 'type' ),
      'parent_items_colon' => __( 'Parent Location:' ),
      'edit_item' => __( 'Edit type' ),
      'update_item' => __( 'Update type' ),
      'add_new_item' => __( 'Add New food type' ),
      'new_item_name' => __( 'New type Name' ),
      'menu_name' => __( 'type' ),
    ),
    // Control the slugs used for this taxonomy
    'rewrite' => array(
      'slug' => 'type', // This controls the base slug that will display before each term
      'with_front' => false, // Don't display the category base before "/locations/"
      'hierarchical' => true // This will allow URL's like "/locations/boston/cambridge/"
    ),
  ));



register_taxonomy('price', 'fooditem', array(
    // Hierarchical taxonomy (like categories)
    'hierarchical' => true,
    // This array of options controls the labels displayed in the WordPress Admin UI
    'labels' => array(
      'name' => _x( 'price', 'price' ),
      'singular_name' => _x( 'price', 'price' ),
      'search_items' =>  __( 'Search Ordrers' ),
      'all_items' => __( 'price' ),
      'parent_items' => __( 'price' ),
      'parent_item_colon' => __( 'Parent Location:' ),
      'edit_item' => __( 'Edit Price' ),
      'update_item' => __( 'Update Order' ),
      'add_new_item' => __( 'Add New Price type' ),
      'new_item_name' => __( 'New price cat' ),
      'menu_name' => __( 'price' ),
    ),
    // Control the slugs used for this taxonomy
    'rewrite' => array(
      'slug' => 'price', // This controls the base slug that will display before each term
      'with_front' => false, // Don't display the category base before "/locations/"
      'hierarchical' => true // This will allow URL's like "/locations/boston/cambridge/"
    ),
  ));

}

add_action( 'init', 'add_custom_taxonomies', 0 );



 // wp_set_post_terms( $post_id, "veg", "type", $append )





//***************create a custom  taxanomy for type////












//create a term under take away taxonomy


// Add some text after the header
add_action( '__after_header' , 'add_promotional_text' );
function add_promotional_text() {
  // If we're not on the home page, do nothing
  // if ( !is_front_page() )
  //   return;
  // Echo the html
  echo "<div>Special offer! June only: Free chocolate for everyone!</div>";
}


add_action( '__after_footer' , 'add_footer_text');

function add_footer_text()
{
  echo "<div>I have added this text using wordpress action hook!!</div>";
}


//adding a filter.. adding more fruits to an existing fruis array 
add_filter('pippin_add_fruits', 'pippin_add_extra_fruits');

function pippin_show_fruits() {
  $fruits = array(
    'apples',
    'oranges',
    'kumkwats',
    'dragon fruit',
    'peaches',
    'durians'
  );
  $list = '<ul>';
 if(has_filter('pippin_add_fruits')) {
    $fruits = apply_filters('pippin_add_fruits', $fruits);
  }
  foreach($fruits as $fruit) :
    $list .= '<li>' . $fruit . '</li>';
  endforeach;
 
  $list .= '</ul>';
 
  return $list;
}

function pippin_add_extra_fruits($fruits) {
  // the $fruits parameter is an array of all fruits from the pippin_show_fruits() function 
  $extra_fruits = array(
    'plums',
    'kiwis',
    'tangerines',
    'pepino melons'
  );
  // combine the two arrays
  $fruits = array_merge($extra_fruits, $fruits);
  return $fruits;
}



// function remove_filter( $tag, $pippin_add_extra_fruits, 1) {
//     global $wp_filter;
//     $r = false;
//     if ( isset( $wp_filter[ $tag ] ) ) 
//     {
//         $r = $wp_filter[ $tag ]->remove_filter( $tag,$pippin_add_extra_fruits, 1 );
//         if ( ! $wp_filter[ $tag ]->callbacks ) {
//             unset( $wp_filter[ $tag ] );
//         }
//     }
 


// add_filter( 'the_content', 'slug_add_copyright' );
// function slug_add_copyright($content) {
// //get current post
// global $post;

// //create copyright notice using post date and site name
// $date = get_the_date('Y',$post->ID);
// $copyright = '<p>Copyright '.$date.' by '.get_bloginfo('name');'.</p>';

// //append copyright notice to post content and return
// $content = $content.$copyright;
// return $content;

// }


?>




